package test.base;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

import java.net.MalformedURLException;
import java.net.URL;

import static pages.BasePage.homeURL;

public class BaseTest {
    private static final String hub = System.getProperty("hub");
    private WebDriver driver = null;

    private void setDriver() {
        URL host = null;
        try {
            host = new URL(hub);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        ChromeOptions options = new ChromeOptions();
        driver = new RemoteWebDriver(host, options);
    }

    protected WebDriver getDriver() {
        return driver;
    }

    @BeforeClass
    public void setUp() throws Exception {
        setDriver();
        driver.manage().window().maximize();
        driver.get(homeURL);
    }

    @AfterClass
    public void quit() {
        driver.quit();
    }
}